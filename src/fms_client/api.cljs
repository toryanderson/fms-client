(ns fms-client.api
  (:require [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [clojure.string :as str]
            [reagent.core :as r]
            [goog.string :as gs]
            [fms-client.android.ui :as ui]))

(defn api-host
  "Return the string for the base host and port needed"
  []
  (let [{:keys [server-host port]} @(subscribe [:get-profile-data])]
    (str "http://" server-host ":" port)))

(defn sanitize-method
  "Convert the method to the appropriate form"
  [m]
  (if-not m
    (throw (ex-info "No m given" {:cause :not-given}))
    (-> m name (clojure.string/upper-case))))

(defn http
  "Make an http-request, expecting a json response"
  [{:as request-map
    :keys [url method token body success-handler error-handler]}]
  (let [fetch-map (clj->js (cond->
                               {:method (sanitize-method method)
                                :headers (cond->
                                             {"Accept" "application/json"
                                              "Content-Type" "application/json"}
                                           (some? token)
                                           (assoc "Authorization" token))}
                             (some? body)
                             (assoc :body (js/JSON.stringify (clj->js body)))))]
    (-> (js/fetch url fetch-map)
        (.then (fn [response]                 
                 (if (not (nil? response))
                   (let [ok (.-ok response)
                         handle-fn (fn [data]
                                     (let [cljdata (js->clj data)]
                                       (dispatch [:set-http-response data])
                                       (if ok
                                         (success-handler cljdata)
                                         (error-handler cljdata))))]                     
                     (-> (.json response)
                         (.then handle-fn))))))
        (.catch (fn [error]
                  (prn error)
                  (error-handler error))))))

(defn id-map
  "Generate a map from `c` in which a specified `id-field` is made the key for each element"
  [id-field coll]
  (into {} (for [c coll] [(get c id-field) c])))


(defn get-event-data
  "Presuming the authorization is already in place, get the event data for the user"
  ([]
   (let [token (get @(subscribe [:get-authorization]) "authToken")]
     (get-event-data token)))
  ([token]
   (let [request-map {:url (str (api-host) "/event")
                      :token token
                      :method "get"
                      :success-handler #(do (println "Got event data")
                                            (dispatch [:set-events (id-map "eventID" %)])
                                            (ui/pop-user-toast))
                      :error-handler #(println "Error getting event data:" %)}]
     (if (not token)
       (throw (ex-info "No token found from authorization" {:cause :no-token}))
       (http request-map)))))

(defn get-family-data
  "Presuming the authorization is already in place, get the family data for the user"
  ([] (let [token (get @(subscribe [:get-authorization]) "authToken")]
        (get-family-data token))
   )
  ([token]
   (let [request-map {:url (str (api-host) "/person")
                      :token token
                      :method "get"
                      :success-handler #(do (println "Got people data")
                                            (dispatch [:set-people (id-map "personID" %)])
                                            (get-event-data token))
                      :error-handler #(println "Error getting family data")}]
     (if (not token)
       (throw (ex-info "No token found from authorization" {:cause :no-token}))
       (http request-map)))))

(defn data-ready?
  "Is the people and event data ready?"
  []
  (let [people (subscribe [:get-people])
        events (subscribe [:get-events])]
    (every? not-empty [@people @events])))
