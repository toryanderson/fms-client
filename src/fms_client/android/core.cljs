(ns fms-client.android.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.events]
            [fms-client.subs]
            [fms-client.android.views.home :refer [home-page]]
            [fms-client.android.views.person :refer [person-page]]
            [fms-client.android.views.settings :refer [settings-page]]
            [fms-client.android.views.filter :refer [filter-page]]
            [fms-client.android.views.search :refer [search-page]]
            [fms-client.android.views.event :refer [event-page]]))
(def ReactNative (js/require "react-native"))
(def app-registry (.-AppRegistry ReactNative))
(def react-navigation (js/require "react-navigation"))
(def create-stack-navigator (.-createStackNavigator react-navigation))
(def create-app-container (.-createAppContainer react-navigation))

(defn header-right-component [press-fn]
  (r/as-element
   [ui/button {:title ""
               :type "clear"
               :on-press press-fn
               :icon (r/as-element
                      [ui/icon {:name "gear"
                                :size 30
                                :color "#666"}])}]))

(def stack-navigator 
  (create-stack-navigator
   (clj->js {:Home (home-page)
             :Settings (settings-page)
             :Filter (filter-page)
             :Search (search-page)
             :Person (person-page)
             :Event (event-page)})
   (clj->js {:initialRouteName "Home"})))

(defn app-root [] [:> (create-app-container stack-navigator) {}])

(defn init []
      (dispatch-sync [:initialize-db])
  (.registerComponent app-registry "fmsClient" #(r/reactify-component app-root)))
