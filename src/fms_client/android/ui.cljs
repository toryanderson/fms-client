(ns fms-client.android.ui
  (:require [reagent.core :as r :refer [atom]]
            [fms-client.data :as d]
            [re-frame.core :refer [dispatch subscribe dispatch-sync]]))

(def react-native (js/require "react-native"))
(def native-elements (js/require "react-native-elements"))
(def icons (js/require "react-native-vector-icons/FontAwesome"))
(def react-native-collapsible (js/require "react-native-collapsible"))
(def collapsible (r/adapt-react-class (.-default react-native-collapsible)))
(def accordion (r/adapt-react-class (.-default (js/require "react-native-collapsible/Accordion"))))
(def icon (r/adapt-react-class (.-default icons)))
(def text (r/adapt-react-class (.-Text react-native)))
(def view (r/adapt-react-class (.-View react-native)))
(def image (r/adapt-react-class (.-Image react-native)))
(def touchable-highlight (r/adapt-react-class (.-TouchableHighlight react-native)))
(def touchable-opacity (r/adapt-react-class (.-TouchableOpacity react-native)))
(def logo-img (js/require "./images/cljs.png"))
(def toast (.-ToastAndroid react-native))
;(def input (r/adapt-react-class (.-TextInput react-native)))
(def input (r/adapt-react-class (.-Input native-elements)))
(def picker (r/adapt-react-class (.-Picker react-native)))
(def pitem (r/adapt-react-class (.. react-native -Picker -Item)))
(def switch (r/adapt-react-class (.-Switch react-native)))
(def button (r/adapt-react-class (.-Button native-elements)))
(def scroll (r/adapt-react-class (.-ScrollView react-native)))
(def searchbar (r/adapt-react-class (.-SearchBar native-elements)))
(def react-native-maps (js/require "react-native-maps"))
(def map-view (r/adapt-react-class (.-default react-native-maps)))
(def map-marker (r/adapt-react-class (.-Marker react-native-maps)))
(def polyline  (r/adapt-react-class (.-Polyline react-native-maps)))


(defn alert [title]
      (.alert (.-Alert react-native) title))

(defn pop-toast
  "Pop up a toast saying `message`"
  [message]
  (.show toast message (.-LONG toast)))

(defn generate-picker
  "Generate a picker"
  [{:as opt-map
    :keys [label selected-value on-value-change options style]}]
  (let [picker-style (merge {:width 110 :height 22} style)]
    [view {:flex-direction "column"}
     [text {:style {:font-weight "600" :text-align "left" :padding-left 5 :font-size 16}}
      label]
     (into [picker (merge {:style picker-style}
                          (select-keys opt-map [:selected-value
                                                :on-value-change]))]
           (for [o options] [pitem o]))]))

(defn pop-user-toast
  "A toast with the user info"
  []
  (let [user-info @(subscribe [:get-profile-data])
        message (if (empty? user-info)
                  "Error obtaining user-info"
                  (str "The family is ready for " (:firstName user-info)
                       " " (:lastName user-info)))]
    (pop-toast message)))

(defn settings-line
  "Generate a line of the settings, with text, explanation- and whatever options"
  [title explanation & rest]
  (let [line-style {:flex-direction "row"
                    :align-content "space-between"
                    :background-color "#E3e3e3"
                    :border-bottom-width 1
                    :margin-bottom 5}
        text-style {:color "#222"
                    :font-size 16}
        explanation-style {:color "#777"
                           :font-size 14}
        row [view {:style line-style}
             [view {:style {:flex-direction "column"
                            :width 230}}
              [text {:style text-style} title]
              [text {:style (merge text-style explanation-style)}
               explanation]]]]
    (into row rest)))

(defn person-block
  "A person-b;ock for search and person activities"
  [person-id navigate]
  (let [people (subscribe [:get-people])
        gender #(-> % (get "gender") {"m" "male" "f" "female"})
        gender-color #(-> (gender %) {"male" "blue"
                                      "female" "red"})
        person-name #(str (% "firstName") " "
                          (% "lastName"))
        person-icon (fn [person]
                      [view {:style {:margin-top 20}}
                       [icon {:name (gender person)
                              :size 18
                              :color (gender-color person)}]])
        person-details (fn [person]
                         [view {:style {:flex-direction "column" :margin 10}}
                          [text
                           {:style {:font-weight "bold" :font-size 14}}
                           (person-name person)]])
        person-block (fn [person]
                       [touchable-opacity {:on-press #(do
                                                        (dispatch-sync [:person/set-selected (get person "personID")])
                                                        (navigate "Person"))}
                        [view {:style {:flex-direction "row"}}
                         [person-icon person]
                         [person-details person]]])]
    [view {:style {:flex-direction "column" :align-items "stretch"
                      :margin-left 10}}
     (when-let [p (get @people person-id)]
       [person-block p])]))

(defn event-block
  "An event-block for search and person activities"
  [event navigate & [icon?]]
  (let [event-id (get event "eventID")]
    [touchable-opacity {:on-press (fn [_]
                                    #_(dispatch [:person/set-selected nil])
                                    (dispatch [:map/set-selected event-id])
                                    (navigate "Event"))}
     [view {:style {:flex-direction "row" :margin 10}}
      (when icon?
        [icon {:name "map-marker"
               :size 18
               :style {:padding-right 5}}])
      [text {:style {:font-size 18}}
       (str (event "eventType") ": " (event "city") ", " (event "country") " (" (event "year") ")")]]]))

(defn person-line
  "Generate a line of the settings, with text, explanation- and whatever options"
  [title explanation & rest]
  (let [line-style {:flex-direction "column"
                    :align-content "space-between"
                    :background-color "#E3e3e3"
                    :border-bottom-width 1
                    :margin-bottom 0}
        text-style {:color "#222"
                    :font-size 18
                    :font-weight "600"}
        explanation-style {:color "#777"
                           :font-size 14}
        row [view {:style line-style}
             [text {:style text-style} title]
             [text {:style (merge text-style explanation-style)}
              explanation]]]
    (into row rest)))

(defn header-button
  "Generate a button for any of `[:settings :filter :search]`, with an on-click according to the passed-in navigator"
  [button-type press-fn & [raw?]]
  (let [button-icons {:settings "gear"
                      :filter "filter"
                      :search "search"}
        btn [button {:title nil
                     :type "clear"
                     :on-press press-fn
                     :icon (r/as-element
                            [icon {:name (or (button-icons button-type)
                                             (throw (ex-info "Invalid button type" {:button-type button-type})))
                                   :size 30
                                   :color "#666"}])}]]
    (if raw?
      btn
      (r/as-element btn))))


(defn show-map?
  "Whether the map-fragment should be shown right now"
  []
  (let [people (subscribe [:get-people])
        events (subscribe [:get-events])]
    (every? not-empty [@people @events])))

(defn generate-filter-switch
  "Generate a filter switch for the filter page"
  [title explanation {:as opt-map
                      :keys [:value :onValueChange]}]
  [settings-line title explanation
   [switch opt-map]])

(defn search-results
  "Area for all search results"
  [navigate]
  (let [search (:text @(subscribe [:get-search]))
        search-results (when (not-empty search) (d/do-search search))
        {:keys [people events]} search-results]
    [scroll 
     (cond-> [view]
       (not-empty people)
       (into (for [person people] [person-block (get person "personID") navigate]))

       (not-empty events)
       (into (for [event events] [event-block event navigate :pin])))]))

(defn collapsible-section
  "Collapse `contents` with touchableheader `title`"
  [title contents]
  (let [collapsed? (atom true)]
    (fn []
      [view
       [touchable-opacity {:on-press #(swap! collapsed? not)}
        [text {:style {:font-weight "600" :font-size 18}}
         title]]
       [collapsible {:collapsed @collapsed?}
        contents]])))
