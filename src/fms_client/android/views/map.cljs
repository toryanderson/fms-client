(ns fms-client.android.views.map
  "NS for map fragment"
  (:require [fms-client.android.ui :as ui]
            [fms-client.data :as d]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]))

(defn ^:private init-selected!
  "Set the initial selection to be the user's earliest event"
  []
  (let [username (:userName @(subscribe [:get-profile-data]))
        user-events (d/events-for-person username)
        earliest-event-id (-> user-events first (get "eventID"))]
    (dispatch-sync [:person/set-selected username])
    (dispatch-sync [:map/set-selected earliest-event-id])))

(defn ^:private get-marker-color
  "Get the appropriate color for an event type"
  [event]
  (let [event-type (event "eventType")
        event-colors {"birth" "#0f0"
                      "marriage" "teal"}
        default "purple"]
    (event-colors event-type default)))

(defn ^:private generate-markers
  "Generate markers for all events"
  []
  (let [events (d/apply-filters)]
    (for [[_ e] events :let [event-id (e "eventID")
                              marker-color (get-marker-color e)
                              marker-map {:title (e "eventType")
                                          :pin-color marker-color
                                          :on-press (fn [_]
                                                      (dispatch [:map/set-selected event-id])
                                                      (dispatch [:person/set-selected
                                                                 (get (d/person-for-event event-id) "personID")]))
                                          :coordinate {:latitude (e "latitude")
                                                       :longitude (e "longitude")}}]]
      [ui/map-marker marker-map])))

(defn ^:private render-line
  "Draw a line connecting event of `:source-id` to `:target-id` or
  target's first event if it's a person, with stroke-width `:width`
  and `:color`. If both of the `-id` are checked as to whether they
  are events or people; if a person, it is connected to the first
  event for that person"
  [{:as line-input
    :keys [source-id target-id width color]
    :or {width 1
         color "red"}}]
  (when (some nil? [source-id target-id])
    (throw (ex-info "Invalid nil to render-line"
                    {:source-id source-id :target-id target-id})))
  (let [source-coords (d/get-ambiguous-coords source-id)
        target-coords (d/get-ambiguous-coords target-id)]
    (when (every? some? [source-coords target-coords])
      [ui/polyline {:stroke-width width
                    :stroke-color color
                    :coordinates (clj->js [source-coords
                                           target-coords])}])))

(defn ^:private generate-spouse-line
  "Draw a line from selected event to the earliest event of the spouse"
  ([]
   (let [event-id @(subscribe [:map/get-selected])]
     (when (not-empty event-id) 
       (generate-spouse-line event-id))))
  ([event-id]
   (let [settings (subscribe [:get-settings])
         spouse-id (-> event-id d/spouse-id-for-event)]
     (when (and
            spouse-id
            (get-in @settings [:spouse-lines :show]))
       [(render-line {:source-id event-id
                      :target-id spouse-id
                      :color (get-in @settings [:spouse-lines :color])})]))))

(defn ^:private family-lines-to-draw
  "Based on filters, which side(s) of the family to render for"
  []
  (->> (select-keys @(subscribe [:get-filters])
                    [:father :mother])
       (filter (complement val))
       (map first)))

(defn ^:private people-lines
  "Generates for `person-id` [[<all-father-people>][<all-mother-people>] ...]
  for people needed for family lines enabled by filters and settings"
  [person-id]
  (map (partial d/get-family-branch person-id)
       (family-lines-to-draw)))

(defn ^:private people-branches
  "Takes results like `people-lines` (e.g. collection of collections)
  and makes each person into [<IDX> <PERSON-ID>]"
  [people-lines]
  (map (partial map-indexed vector) people-lines))

(defn ^:private generate-family-tree-lines
  "Generate the family tree line, which starts at current event and then proceeds to birth events of parents recursively"
  ([]
   (let [event-id @(subscribe [:map/get-selected])]
     (when (not-empty event-id) 
       (generate-family-tree-lines event-id))))
  ([event-id]
   (when (get-in @(subscribe [:get-settings]) [:family-tree-lines :show])
     (let [family-tree-line-color (get-in @(subscribe [:get-settings]) [:family-tree-lines :color])
           person-id ((d/person-for-event event-id) "personID")
           person-branches (-> person-id people-lines
                               (->> (map #(into [event-id] %))) ;; to start with the selected event
                               people-branches)
           scale (partial * 3)]
       (for [[[idx source-id]
              [_ target-id] :as branch] (mapcat #(partition 2 1 %) person-branches)]
         (render-line {:source-id source-id
                       :target-id target-id
                       :width (-> (count branch)
                                  inc
                                  (- idx)
                                  scale)
                       :color family-tree-line-color}))))))

(defn ^:private generate-life-story-lines
  "Generate the life story lines of each event to the previous, ordered chronologically"
  []
  (let [selected-event-id (subscribe [:map/get-selected])]
    (if (empty? @selected-event-id)
      (init-selected!)
      (let [event (@(subscribe [:get-events]) @selected-event-id)
            person (d/person-for-event @selected-event-id)]
        (when (get-in @(subscribe [:get-settings]) [:life-lines :show])
          (let [life-line-color (get-in @(subscribe [:get-settings]) [:life-lines :color])
                person-id ((d/person-for-event @selected-event-id) "personID")
                person-events (map #(get % "eventID") (d/events-for-person person-id))]
            (for [[source-id target-id] (partition 2 1 person-events)]
              (render-line {:source-id source-id
                            :target-id target-id
                            :width 2
                            :color life-line-color}))))))))

(defn ^:private detail-area
  "produce the person-details for the currently selected `event-id`. Assumes existence of select-event-id has been checked."
  [event-id]
  (let [selected-event-id (subscribe [:map/get-selected])]
    (if (empty? @selected-event-id)
      (init-selected!)
      (let [event (@(subscribe [:get-events]) @selected-event-id)
            person (d/person-for-event @selected-event-id)
            gender (-> person (get "gender") {"m" "male" "f" "female"})
            gender-color (-> gender {"male" "blue"
                                     "female" "red"})
            person-name (str (person "firstName") " "
                             (person "lastName"))]
        [ui/view {:style {:flex-direction "row" :align-items "stretch"
                          :margin-left 10}}
         [ui/view {:style {:margin-top 20}}
          [ui/icon {:name gender
                    :size 38
                    :color gender-color}]]
         [ui/view {:style {:flex-direction "column" :margin 10}}
          [ui/text
           {:style {:font-weight "bold" :font-size 24}}
           person-name]
          [ui/text {:style {:font-size 18}}
           (str (event "eventType") ": " (event "city") ", " (event "country") " (" (event "year") ")")]]]))))

(defn ^:private get-selected-region
  []
  (when-let [selected @(subscribe [:map/get-selected])] 
    (let [selected-region (merge (d/get-coords selected)
                                 {:latitudeDelta 100
                                  :longitudeDelta 100})]
      {:region selected-region})))

(defn map-fragment
  "Map fragment"
  [navf]
  (let [map-type (:map-type @(subscribe [:get-settings]))
        map-opts (merge
                  {:style {:height "83%"}
                   :map-type (or map-type "standard")}
                  (get-selected-region))
        gmapview #(-> [ui/map-view map-opts]                      
                      (into (generate-markers))
                      (into (generate-spouse-line))
                      (into (generate-family-tree-lines))
                      (into (generate-life-story-lines)))]
    gmapview
    [ui/view
     [gmapview]
     [ui/touchable-opacity {:on-press #(navf "Person")}
      [detail-area]]]))

