(ns fms-client.android.views.settings
  "NS for setting options"
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.api :as api]
            [goog.object :as gobj]))

(def colors [{:value "green"
              :label "Green"}
             {:value "blue"
              :label "Blue"}
             {:value "red"
              :label "Red"}])

(def map-types [{:value "standard"
                 :label "Standard"}
                {:value "hybrid"
                 :label "Hybrid"}
                {:value "satellite"
                 :label "Satellite"}
                {:value "terrain"
                 :label "Terrain"}])

(defn resync-login
  "Submit registration request, next requesting family data"
  []
  (let [payload @(subscribe [:get-profile-data])
        request-map {:url (str (api/api-host) "/user/login")
                     :method "post"
                     :body payload
                     :success-handler (fn [data]
                                        (dispatch-sync [:set-authorization data])
                                        (api/get-family-data))
                     :error-handler #(do (println (str ">> error\n" (prn-str (ex-data %))))
                                         (ui/pop-toast "Your submission failed."))}]
    (prn ["Request-map" request-map])
    (api/http request-map)))

(defn settings-raw [props]
  (let [current-settings (subscribe [:get-settings])
        navigate (get-in (js->clj props) [:navigation "navigate"])]
    (fn []
      [ui/scroll {:flex-direction "column"}
       (let [k :life-lines]
         [ui/settings-line "Life Story Lines"
          "SHOW LIFE STORY LINES"
          [ui/generate-picker {:label nil
                               :selected-value (get-in @current-settings [k :color])
                               :on-value-change #(dispatch [:set-setting [k :color] %])
                               :options colors}]
          [ui/switch {:value (get-in @current-settings [k :show])
                      :onValueChange #(dispatch [:set-setting [k :show] %])}]])
       (let [k :family-tree-lines]
         [ui/settings-line "Family Tree Lines"
          "SHOW TREE LINES"
          [ui/generate-picker {:label nil
                               :selected-value (get-in @current-settings [k :color])
                               :on-value-change #(dispatch [:set-setting [k :color] %])
                               :options colors}]
          [ui/switch {:value (get-in @current-settings [k :show])
                      :onValueChange #(dispatch [:set-setting [k :show] %])}]])
       (let [k :spouse-lines]
         [ui/settings-line "Spouse Lines"
          "SHOW SPOUSE LINES"
          [ui/generate-picker {:label nil
                               :selected-value (get-in @current-settings [k :color])
                               :on-value-change #(dispatch [:set-setting [k :color] %])
                               :options colors}]
          [ui/switch {:value (get-in @current-settings [k :show])
                      :onValueChange #(dispatch [:set-setting [k :show] %])}]])
       (let [k :map-type]
         [ui/settings-line "Map Type"
          "BACKGROUND DISPLAY ON MAP"
          [ui/generate-picker {:label nil
                               :selected-value (get-in @current-settings [k])
                               :on-value-change #(dispatch [:set-setting [k] %])
                               :style {:width 150}
                               :options map-types}]])
       ;; TODO below
       [ui/touchable-opacity {:on-press (fn [_]
                                          (resync-login)
                                          (navigate "Home"))}
        [ui/settings-line "Re-sync Data"
         "FROM FAMILYMAP SERVICE"]]
       [ui/touchable-opacity {:on-press (fn [_]
                                          (dispatch [:initialize-db])
                                          (navigate "Home"))}
        [ui/settings-line "Logout"
         "RETURNS TO LOGIN SCREEN"]]
       
       ])))

(defn settings-page
  "Page of app settings"
  []
  (let [reactified (r/reactify-component (fn [_] settings-raw))]
    (doto reactified
      (gobj/set "navigationOptions"
                #js {:headerTitle "Family Map: Settings"}))))

