(ns fms-client.android.views.filter
  "NS for filter settings"
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.api :as api]
            [clojure.string :as str]
            [goog.object :as gobj]))

(defn event-types
  "Gather all event-types from loaded events"
  []
  (let [events (subscribe [:get-events])]
    (->> @events
         vals
         (map #(get % "eventType"))
         (map str/lower-case)
         set)))

(defn resync-login
  "Submit registration request, next requesting family data"
  []
  (let [payload @(subscribe [:get-profile-data])
        request-map {:url (str (api/api-host) "/user/login")
                     :method "post"
                     :body payload
                     :success-handler (fn [data]
                                        (dispatch-sync [:set-authorization data])
                                        (api/get-family-data))
                     :error-handler #(do (println (str ">> error\n" (prn-str (ex-data %))))
                                         (ui/pop-toast "Your submission failed."))}]
    (prn ["Request-map" request-map])
    (api/http request-map)))

(defn upcase
  "Capitalize the first letters of each word"
  [s]
  (->> (str/split s #"\W")
       (map str/capitalize)
       (str/join " ")))


(defn event-type-toggles
  "Toggle by each event type"
  []
  (let [current-filters (subscribe [:get-filters])
        view [ui/view {}]]
    (into #_scroll view
          (for [etype (event-types)
                :let [title (upcase etype)
                      explanation (str/upper-case
                                   (str "filter by " etype))]]
            [ui/generate-filter-switch title explanation
             {:value (not (get-in @current-filters [:event-type etype]))
              :onValueChange #(dispatch-sync [:toggle-filter [:event-type etype]])}]))))

(defn gender-toggles
  []
  (let [current-filters (subscribe [:get-filters])
        explanation "FILTER EVENTS BASED ON GENDER"]
    [ui/view
     [ui/generate-filter-switch
      "MALE EVENTS"
      explanation
      {:value (not (get-in @current-filters [:male]))
       :on-value-change #(dispatch-sync [:toggle-filter [:male]])}]
     [ui/generate-filter-switch
      "FEMALE EVENTS"
      explanation
      {:value (not (get-in @current-filters [:female]))
       :on-value-change #(dispatch-sync [:toggle-filter [:female]])}]]))

(defn family-side-toggles
  []
  (let [current-filters (subscribe [:get-filters])
        explanation #(str "FILTER EVENTS BY "
                          % "'S SIDE OF THE FAMILY")]
    [ui/view
     [ui/generate-filter-switch
      "FATHER EVENTS"
      (explanation "FATHER")
      {:value (not (get-in @current-filters [:father]))
       :on-value-change #(dispatch-sync [:toggle-filter [:father]])}]
     [ui/generate-filter-switch
      "MOTHER EVENTS"
      (explanation "MOTHER")
      {:value (not (get-in @current-filters [:mother]))
       :on-value-change #(dispatch-sync [:toggle-filter [:mother]])}]]))

(defn filter-raw [props]
  (fn []
    [ui/scroll {:flex-direction "column"}
     [event-type-toggles]
     [gender-toggles]
     [family-side-toggles]]))

(defn filter-page
  "Page of app filter"
  []
  (let [reactified (r/reactify-component (fn [_] filter-raw))]
    (doto reactified
      (gobj/set "navigationOptions"
                #js {:headerTitle "Family Map: Filter"}))))

