(ns fms-client.android.views.event
  "NS for homepage/map fragment"
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.api :as api]
            [fms-client.events]
            [fms-client.subs]
            [fms-client.android.views.map :refer [map-fragment]]
            [goog.object :as gobj]
            ))

(def ReactNative (js/require "react-native"))
(def app-registry (.-AppRegistry ReactNative))

(defn nav-options
  "Produce the #js nav-options that set the header-bar"
  [props]
  (clj->js {:headerTitle "Family Map"}))

(defn _event-component [props]
  (let [navf (get-in (js->clj props) [:navigation "navigate"])]
    [map-fragment navf]))

(defn event-component []
  (r/reactify-component _event-component))

(defn event-page []
  (doto (event-component)
    (gobj/set "navigationOptions" nav-options)))
