(ns fms-client.android.views.home
  "NS for homepage/map fragment"
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.api :as api]
            [fms-client.events]
            [fms-client.subs]
            [fms-client.android.views.map :refer [map-fragment]]
            [goog.object :as gobj]
            ))

(def ReactNative (js/require "react-native"))
(def app-registry (.-AppRegistry ReactNative))

(defn home-form []
  (let [label-style {:width 200}
        data (subscribe [:get-profile-data])
        gender (subscribe [:get-profile-data :gender])]
    (fn []
      (let [gender-opts {:label "Gender"
                         :selected-value @gender
                         :on-value-change #(dispatch [:set-profile-value :gender %])
                         :options  [{:value "m"
                                     :label "Male"}
                                    {:value "f"
                                     :label "Female"}]}]
        [ui/view         
         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "Server Name/Host"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :server-host value]))
                    :placeholder "<10.0.2.2>"
                    :value (:server-host @data)
                    :style {:width 30}
                    }]
         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "Port"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :port value]))
                    :placeholder "<3000>"
                    :value (:port @data)
                    :style {:width 30}
                    }]
         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "Username"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :userName value]))
                    :placeholder "<USERNAME>"
                    :value (:userName @data)
                    :style {:width 30}
                    }]
         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "Password"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :password value]))
                    :value (:password @data)
                    :placeholder "<PASSWORD>"
                    :style {:width 30}
                    }]

         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "First Name"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :firstName value]))
                    :value (:firstName @data)
                    :placeholder "<FIRST NAME>"
                    :style {:width 30}
                    }]
         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "Last Name"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :lastName value]))
                    :value (:lastName @data)
                    :placeholder "<LAST NAME>"
                    :style {:width 30}
                    }]
         [ui/input {:auto-correct false
                    :clear-button-mode "always"
                    :label "Email"
                    :label-style label-style
                    :on-change-text (fn [value] (dispatch [:set-profile-value :email value]))
                    :value (:email @data)
                    :placeholder "<EMAIL>"
                    :style {:width 30}
                    }]
         [ui/generate-picker gender-opts]]))))

(defn submit-register
  "Submit registration request, next requesting family data"
  []
  (let [payload @(subscribe [:get-profile-data])
        {:keys [server-host port]} payload
        request-map {:url (str (api/api-host) "/user/register")
                     :method "post"
                     :body payload
                     :success-handler (fn [data]
                                        (dispatch [:set-authorization data])
                                        (api/get-family-data))
                     :error-handler #(do (println (str ">> error\n" (prn-str (ex-data %))))
                                         (ui/pop-toast "Your submission failed."))}]
    (ui/pop-toast "Registering...")
    (api/http request-map)))

(defn submit-login
  "Submit registration request, next requesting family data"
  []
  (let [payload @(subscribe [:get-profile-data])
        request-map {:url (str (api/api-host) "/user/login")
                     :method "post"
                     :body payload
                     :success-handler (fn [data]
                                        (dispatch-sync [:set-authorization data])
                                        (api/get-family-data))
                     :error-handler #(do (println (str ">> error\n" (prn-str (ex-data %))))
                                         (ui/pop-toast "Your submission failed."))}]
    (api/http request-map)))


(def home-page-raw
  (let [greeting (subscribe [:get-greeting])]
    (fn []
      (let [form-vals @(subscribe [:get-profile-data])
            disabled? (some (comp empty? second) form-vals)
            bgcolor (if disabled? "#999" "#0000cd")
            textcolor (if disabled? "#696969" "white")]
        [ui/scroll {:style {:flex-direction "column" :margin 40 #_#_:align-items "center"}}
         [ui/text {:style {:font-size 30 :font-weight "100" :margin-bottom 20 :text-align "center"}} @greeting]         
         [home-form]
         [ui/view {:style {:flex-direction "row" :margin 40 :align-items "center"}}
          [ui/touchable-highlight {:style {:background-color bgcolor :padding 10 :border-radius 5 :margin 5}
                                   :disabled disabled?
                                   :on-press submit-register}
           [ui/text {:style {:color textcolor :text-align "center" :font-weight "bold"}} "Register"]]
          [ui/touchable-highlight {:style {:background-color bgcolor :padding 10 :border-radius 5 :margin 5}
                                   :disabled disabled?
                                   :on-press submit-login}
           [ui/text {:style {:color textcolor :text-align "center" :font-weight "bold"}} "Login"]]]]))))

(defn nav-options
  "Produce the #js nav-options that set the header-bar"
  [props]
  ;; (when (api/data-ready?)
  ;;   (println "Re-evaluating nav-options because data is ready"))
  (let [navigate (get-in (js->clj props) ["navigation" "navigate"])
        map-buttons  [[ui/header-button :search #(navigate "Search")]
                      [ui/header-button :filter #(navigate "Filter")]
                      [ui/header-button :settings #(navigate "Settings")]]
        full-nav-right #(cond-> [ui/view {:style {:flex-direction "row" :align-items "center"}}]
                          :always (into map-buttons))]
    (clj->js {:headerTitle "Family Map"
              :headerRight (r/as-element (full-nav-right))})))

(defn _home-component [props]
  (if (api/data-ready?)
    (let [navf (get-in (js->clj props) [:navigation "navigate"])]
      [map-fragment navf])
    [home-page-raw]))

(defn home-component []
  (r/reactify-component _home-component))

(defn home-page []
  (doto (home-component)
    (gobj/set "navigationOptions" nav-options)))
