(ns fms-client.android.views.person
  "NS for the Person Activity"
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.data :as d]
            [fms-client.events]
            [fms-client.subs]
            [goog.object :as gobj]
            ))

(defn nav-options
  "Nav options for the person page"
  [_props]
  (clj->js {:headerTitle "Family Map: Person Details"}))

(defn text-detail
  "Text detail with an under-message"
  [main sub]
  [ui/view {:style {:flex-direction "column" :margin 10}}
   [ui/text ]]
  )


(defn life-event-section
  "Events section of person view"
  [person-id navigate]
  (let [life-events (d/events-for-person person-id)]
    (into [ui/view]
          (for [event life-events :let [event-id (get event "eventID")]]
            [ui/event-block event navigate :icon]))))

(defn family-section
  "Family section of person view"
  [person-id navigate]
  (let [people (subscribe [:get-people])
        spouse (@people (d/spouse-id-for-person person-id))
        {:strs [fatherID motherID]
         :as parents} (d/parent-ids-for-person person-id)
        child (d/child-for-person person-id)
        gender #(-> % (get "gender") {"m" "male" "f" "female"})
        gender-color #(-> (gender %) {"male" "blue"
                                      "female" "red"})
        person-name #(str (% "firstName") " "
                          (% "lastName"))
        person-icon (fn [person]
                      [ui/view {:style {:margin-top 20}}
                       [ui/icon {:name (gender person)
                                 :size 18
                                 :color (gender-color person)}]])
        person-details (fn [person relation-s]
                         [ui/view {:style {:flex-direction "column" :margin 10}}
                          [ui/text
                           {:style {:font-weight "bold" :font-size 14}}
                           (person-name person)]
                          [ui/text
                           {:style {:font-weight "bold" :font-size 14}}
                           relation-s]])
        person-block (fn [person relation-s]
                       [ui/touchable-opacity {:on-press #(do
                                                           (println "I want my selected person to be: " (get person "personID"))
                                                           (dispatch [:person/set-selected (get person "personID")]))}
                        [ui/view {:style {:flex-direction "row"}}
                         [person-icon person]
                         [person-details person relation-s]]])]
    
    [ui/view {:style {:flex-direction "column" :align-items "stretch"
                      :margin-left 10}}
     (when-let [f (@people fatherID)]
       [person-block f "Father"])
     (when-let [m (@people motherID)]
       [person-block m "Mother"])
     (when-let [s spouse]
       [person-block s "Spouse"])
     (when-let [c child]
       [person-block c "Child"])]))

(defn _person-component [props]
  (let [navigate (get-in (js->clj props) [:navigation "navigate"])
        push (get-in (js->clj props) [:navigation "push"])
        selected-person-id (subscribe [:person/get-selected])
        people (subscribe [:get-people])
        person (@people @selected-person-id)
        event-section (fn []
                       [ui/collapsible-section
                        "Events"
                        [life-event-section @selected-person-id navigate]])
        collapsible-family-section (fn []
                        [ui/collapsible-section
                         "Family"
                         [family-section @selected-person-id push]])]
    [ui/scroll {:style {:flex-direction "column" :margin 10}}
     [ui/person-line
      (person "firstName")
      "FIRST NAME"]
     [ui/person-line
      (person "lastName")
      "LAST NAME"]
     [ui/person-line
      (-> person (get "gender") {"m" "Male" "f" "Female"})
      "GENDER"]
     [event-section]
     [collapsible-family-section]]))

(defn person-component []
  (r/reactify-component _person-component))

(defn person-page
  []
  (doto (person-component)
    (gobj/set "navigationOptions" nav-options))
  )


