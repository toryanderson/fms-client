(ns fms-client.android.views.search
  "NS for Search activity"
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [fms-client.android.ui :as ui]
            [fms-client.api :as api]
            [clojure.string :as str]
            [goog.object :as gobj]))

(defn search-raw [props]
  (let [navigate (get-in (js->clj props) [:navigation "navigate"])
        search (subscribe [:get-search])]
    (fn []
      (let []
        [ui/scroll {:flex-direction "column"}
         [ui/searchbar {:placeholder "Search..."
                        :value (:text @search)
                        :on-change-text #(dispatch-sync [:set-search %])}]
         [ui/search-results navigate]]))))

(defn search-page
  "Page of app filter"
  []
  (let [reactified (r/reactify-component (fn [_] search-raw))]
    (doto reactified
      (gobj/set "navigationOptions"
                #js {:headerTitle "Family Map: Search"}))))

