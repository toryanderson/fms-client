(ns fms-client.events
  (:require
   [re-frame.core :refer [reg-event-db after reg-event-fx]]
   [clojure.spec.alpha :as s]
   [fms-client.db :as db :refer [app-db]]))

;; -- Interceptors ------------------------------------------------------------
;;
;; See https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md
;;
(defn check-and-throw
  "Throw an exception if db doesn't have a valid spec."
  [spec db [event]]
  (when-not (s/valid? spec db)
    (let [explain-data (s/explain-data spec db)]
      (throw (ex-info (str "Spec check after " event " failed: " explain-data) explain-data)))))

(def validate-spec
  (if goog.DEBUG
    (after (partial check-and-throw ::db/app-db))
    []))

;; -- Handlers --------------------------------------------------------------

(reg-event-db
 :set-greeting
 validate-spec
 (fn [db [_ value]]
   (assoc db :greeting value)))

(reg-event-db
 :set-current-view
 validate-spec
 (fn [db [_ value]]
   (assoc db :current-view value)))

(reg-event-db
 :set-profile-value
 validate-spec
 (fn [db [_ k v]]
   (assoc-in db [:profile-values k] v)))

(reg-event-db
 :set-authorization
 validate-spec
 (fn [db [_ vmap]]
   (assoc db :authorization vmap)))

(reg-event-db
 :set-http-response
 validate-spec
 (fn [db [_ v]]
   (assoc db :http-response v)))

(reg-event-db
 :set-people
 validate-spec
 (fn [db [_ v]]
   (assoc db :people v)))

(reg-event-db
 :set-events
 validate-spec
 (fn [db [_ v]]
   (assoc db :events v)))

;;;;;;;;;;;;;;;;
;; NAV EVENTS ;;
;;;;;;;;;;;;;;;;

(reg-event-db
 :set-setting
 validate-spec
 (fn [db [_ ks v]]
   (let [ks (cond-> ks
              (keyword? ks) vector) ]
     (assoc-in db (into [:settings] ks) v))))

(reg-event-db
 :map/set-selected
 validate-spec
 (fn [db [_ event-id]]
   (assoc db :map/selected event-id)))

(reg-event-db
 :person/set-selected
 validate-spec
 (fn [db [_ person-id]]
   (assoc db :person/selected person-id)))

(reg-event-db
 :toggle-filter
 validate-spec
 (fn [db [_ filter-path]]
   (update-in db
              (into [:filters] filter-path)
              not)))

(reg-event-db
 :set-search
 validate-spec
 (fn [db [_ s]]
   (assoc-in db [:search :text] s)))
;;;;;;;;;;;;;
;; init-db ;;
;;;;;;;;;;;;;
(reg-event-db
  :initialize-db
  validate-spec
  (fn [_ _]
    app-db))
