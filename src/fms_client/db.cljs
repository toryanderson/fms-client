(ns fms-client.db
  (:require [clojure.spec.alpha :as s]))

;; spec of app-db
(s/def ::greeting string?)
(s/def ::current-view keyword?)
(s/def ::authorization map?)
(s/def ::http-response some?)
(s/def ::people coll?)
(s/def ::events coll?)
(s/def ::profile-values map?)
(s/def ::settings map?)
(s/def ::filters map?)
(s/def ::search map?)
(s/def :map/selected #(or (nil? %) (string? %)))
(s/def :person/selected #(or (nil? %) (string? %)))
(s/def ::app-db
  (s/keys :req [:map/selected :person/selected]
          :req-un [::greeting ::current-view ::authorization ::http-response ::profile-values ::people ::settings ::filters]))

;; initial state of app-db

;; initial state of app-db
(def app-db {:greeting "Family Map"
             :current-view :home
             :authorization {:firstName ""
                             :lastName ""
                             :token ""}
             :http-response []
             :people []
             :events []
             :person/selected nil
             :map/selected nil
             :filters {:event-type nil
                       :father nil
                       :mother nil
                       :male nil
                       :female nil}
             :profile-values {:server-host "10.0.2.2"
                              :port "3000"
                              :userName "user"
                              :password "pass"
                              :firstName "T"
                              :lastName "A"
                              :email "x@x.co"
                              :gender "m"}
             :search {:text ""
                      :results nil}
             :settings {:life-lines {:show true
                                     :color "blue"}
                        :family-tree-lines {:show true
                                            :color "green"}
                        :spouse-lines {:show true
                                       :color "red"}
                        :map-type "standard"}})

