(ns fms-client.data
  "ns for data manipulation functions"
  (:require [re-frame.core :refer [dispatch subscribe]]
            [clojure.string :refer [lower-case join]]))

(defn person-id-for-event
  "Given an event-id, get the personID associated with that event"
  [event-id]
  (let [events @(subscribe [:get-events])]
    (get-in events [event-id "personID"])))

(defn person-for-event
  "Get the whole person object for an `event-id`"
  [event-id]
  (let [people (subscribe [:get-people])]
    (->> event-id person-id-for-event (get @people))))

(defn filtered-gender?
  "True if the filters block the gender of the event person"
  [event]
  (let [filters @(subscribe [:get-filters])
        filtered-genders (->> (select-keys filters [:male :female])
                              (filter val)
                              (map first)
                              (map (comp first name)) ;; have to use a seperate map here because map is nil-safe
                              set)
        event-id (get event "eventID")]
    (-> (person-for-event event-id)
        (get "gender")
        filtered-genders)))

(defn filtered-event-type?
  "True if the filters block the case-insensitive event-type of the event"
  [event]
  (let [filters @(subscribe [:get-filters])
        filtered-events (->> (:event-type filters)
                             (filter val)
                             (map (comp lower-case first))
                              set)
        event-type (lower-case (get event "eventType"))]
    (filtered-events event-type)))

(defn apply-filters
  "Apply the filters to the given events. Works on both raw event collections and indexed ones."
  ([] (apply-filters @(subscribe [:get-events])))
  ([events]
   (let [indexed-events-map? (map? (-> events first second))
         get-event #(if indexed-events-map? (second %) %)]
     (->> events
          (remove (comp filtered-gender? get-event))
          (remove (comp filtered-event-type? get-event))
          (into {})))))

(defn spouse-id-for-person
  [person-id]
  (when person-id
    (let [people (subscribe [:get-people])]
      (-> @people (get person-id) (get "spouseID")))))

(defn spouse-id-for-event
  "Get the spouse id of the person of an event"
  [event-id]
  (when event-id
    (if-let [sid ((person-for-event event-id) "spouseID")]
      sid
      (println "no spouse found"))))

(defn parent-ids-for-person
  "Get the `{:father xyz :mother zyx}` for person"
  [person-id]
  (when person-id
    (let [people @(subscribe [:get-people])]
      (-> person-id people (select-keys ["fatherID" "motherID"])))))

(defn event-comparator
  "Sort by:
  1. birth always first
  2. then by year
  3. then by eventType alphabetically"
  [e1 e2]
  (let [[et1 et2 :as event-types]
        (map (comp lower-case #(get % "eventType")) [e1 e2])
        [y1 y2] (map #(get % "year") [e1 e2]) ]
    (cond
      (= e1 e2) 0
      ;; 1.a Prefer birth events
      (and (not= et1 et2)
           (some #{"birth"} event-types))
      (condp = "birth"
        et1 -1
        et2 1)
      ;; 1.b disprefer death events
      (and (not= et1 et2)
           (some #{"death"} event-types))
      (condp = "death"
        et1 1
        et2 -1)
      ;; 2. prefer year
      (not= y1 y2) (compare y1 y2)
      ;; 3. sort by event type alphabetically
      :else (compare et1 et2))))


(defn events-for-person
  "Get all events for person-id, sorted by date, with birth always first"
  [person-id]
  (let [events (vals (apply-filters))]
    (->> events
         (filter (fn [event] (= person-id (event "personID"))))
         (sort event-comparator))))

(defn child-for-person
  "Get a person's child"
  [person-id]
  (let [people (subscribe [:get-people])
        child-filter (fn [parent-id child]
                       (or (= (child "fatherID") parent-id)
                           (= (child "motherID") parent-id)))]
    (first (filter (partial child-filter person-id) (vals @people)))))


(defmulti get-coords
  "Get coordinates from event-map or event-id"
  (fn [event] (cond
                (map? event) :event-map
                (string? event) :id)))

(defmethod get-coords :event-map
  [event-map]
  (select-keys event-map ["longitude" "latitude"]))

(defmethod get-coords :id
  [event-id]
  (let [events (subscribe [:get-events])]
    (get-coords (@events event-id))))

(defn get-family-branch
  "Get all the family of `line` =  `:father` or `:mother` for `person-id`.
  It does not include the person themselves."
  [person-id line]
  (let [all-people @(subscribe [:get-people])
        linek (-> line {:father "fatherID"
                        :mother "motherID"})]
    (loop [current person-id
           found-people []]
      (if-let [found (get-in all-people [current linek])]
        (recur found (conj found-people found))
        found-people))))

(defn get-fathers
  "Recursively get all the fathers of person-id"
  [person-id]
  (get-family-branch person-id :father))

(defn get-mothers
  "Recursively get all the mothers of person-id"
  [person-id]
  (get-family-branch person-id :mother))

(defn get-ambiguous-coords
  "Get the coordinates for the given id. If it a person-id, get the id of that person's first (earliest) event."
  [id]
  (let [events (apply-filters)
        event (or (->> id (get events))
                  (->> id events-for-person first))
        event-id (get event "eventID")]
    (when event 
      (get-coords event-id))))

(defn filter-person-by-name
  "True if `person`'s name matches regex `s`"
  [rgs person]
  (let [person-name (lower-case
                     (str (person "firstName") " "
                          (person "lastName")))]
    (re-find rgs person-name)))

(defn filter-event-by-pattern
  "True if `event` has country, city, year, or type matching string"
  [rgs event]
  (let [match-fields (select-keys event ["country" "city" "eventType" "year"])
        match-str (->> (vals match-fields)
                       (map (comp lower-case str))
                       (join " " ))]
    (re-find rgs match-str)))

(defn do-search
  "The specified string `s` is searched for in people’s first and last
  names, and in event’s countries, cities, event types, and
  years. Case is ignored. All matching people and events (that aren't
  filtered) are listed in the search result, as shown below."
  [s]
  (let [people (vals @(subscribe [:get-people]))
        events (vals (apply-filters))
        pattern (-> s lower-case re-pattern)]
    {:people (filter (partial filter-person-by-name pattern) people)
     :events (filter (partial filter-event-by-pattern pattern) events)}))
