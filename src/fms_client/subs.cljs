(ns fms-client.subs
  (:require [re-frame.core :refer [reg-sub subscribe]]))

(reg-sub
  :get-greeting
  (fn [db _]
    (:greeting db)))

(reg-sub
 :get-current-view
 (fn [db _]
   (-> db :current-view)))

(reg-sub
 :get-profile-data
 (fn [db [_ k]]
   (cond-> (:profile-values db)
     k k)))

(reg-sub
 :get-authorization
 (fn [db _]
   (:authorization db)))

(reg-sub
 :get-http-response
 (fn [db _]
   ( :http-response db)))

(reg-sub
 :get-people
 (fn [db _]
   (:people db)))

(reg-sub
 :get-events
 (fn [db _]
   (:events db)))

(reg-sub
  :nav/stack-state
  (fn [db [_ route-name]]
    (get-in db [:nav/stack-state (keyword "nav.routeName" route-name)])))

(reg-sub
 :nav/settings-click
 (fn [db [_]]
   (get-in db [:nav/settings-click-fn])))

(reg-sub
 :get-settings
 (fn [db _]
   (get-in db [:settings])))

(reg-sub
 :map/get-selected
 (fn [db _]
   (get db :map/selected)))

(reg-sub
 :person/get-selected
 (fn [db _]
   (get db :person/selected)))

(reg-sub
 :get-filters
 (fn [db _]
   (get-in db [:filters])))

(reg-sub
 :get-search
 (fn [db _]
   (get-in db [:search])))
